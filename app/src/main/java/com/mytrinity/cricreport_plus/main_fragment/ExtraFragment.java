package com.mytrinity.cricreport_plus.main_fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.mytrinity.cricreport_plus.Login;
import com.mytrinity.cricreport_plus.R;
import com.mytrinity.cricreport_plus.wallet_pages.RecentTransaction;

import static android.content.Context.MODE_PRIVATE;


public class ExtraFragment extends Fragment {

    View layout;
    LinearLayout recentTransaction_layout;
    TextView txtname,txtmobile;
    LinearLayout logoutLayout;
    public static final String Default = "N/A";
    String uidHold,holdnm,holdmobile;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout= inflater.inflate(R.layout.fragment_blank, container, false);
        recentTransaction_layout=layout.findViewById(R.id.recentTransaction_layout);
        logoutLayout = layout.findViewById(R.id.logoutLayout);
        txtname= layout.findViewById(R.id.txtname);
        txtmobile= layout.findViewById(R.id.txtmobile);

        SharedPreferences prefs = getActivity().getSharedPreferences("userDetail", MODE_PRIVATE);
        uidHold = prefs.getString("uid", Default);
        holdnm = prefs.getString("name", Default);
        holdmobile = prefs.getString("mobile", Default);

        if (uidHold != Default) {

            txtname.setText(holdnm);
            txtmobile.setText(holdmobile);
            logoutLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            logoutLayout.setVisibility(View.GONE);
        }



        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutClick();
            }
        });


        recentTransaction_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RecentTransaction.class));
            }
        });
        return layout;

    }

    public void logoutClick() {
        FirebaseAuth.getInstance().signOut();
//        startActivity(new Intent(getActivity(), Login.class));
//        getActivity().finish();

        SharedPreferences preferences =getActivity().getSharedPreferences("userDetail", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        startActivity(new Intent(getActivity(), Login.class));
        getActivity().finish();

    }
}