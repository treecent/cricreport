package com.mytrinity.cricreport_plus.main_fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mytrinity.cricreport_plus.MainActivity;
import com.mytrinity.cricreport_plus.R;
import com.mytrinity.cricreport_plus.fragments_mymatches.LiveMatches;
import com.mytrinity.cricreport_plus.fragments_mymatches.RecentMatches;
import com.mytrinity.cricreport_plus.fragments_mymatches.UpcomingMatches;

public class MyMatchesFragment extends Fragment {

    View layout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_mymatches, container, false);
        init();
        return layout;
    }

    public void init()
    {
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getActivity().getSupportFragmentManager());
        ViewPager mViewPager = (ViewPager) layout.findViewById(R.id.container_mymatches);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) layout.findViewById(R.id.tabs_mymatches);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setCurrentItem(0);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:

                    return new UpcomingMatches();

                case 1:

                    return new LiveMatches();

                case 2:

                    return new RecentMatches();

                default:

                    return null;
            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Upcoming";
                case 1:
                    return "Live";
                case 2:
                    return "Recent";

            }
            return null;
        }
    }
}