package com.mytrinity.cricreport_plus.main_fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.mytrinity.cricreport_plus.Login;
import com.mytrinity.cricreport_plus.MainActivity;
import com.mytrinity.cricreport_plus.R;
import com.mytrinity.cricreport_plus.main_fragment.list_allmatches.Adapter_allmatches;
import com.mytrinity.cricreport_plus.main_fragment.list_allmatches.Interface_JoinClick;
import com.mytrinity.cricreport_plus.main_fragment.list_allmatches.Interface_allmatches;
import com.mytrinity.cricreport_plus.main_fragment.list_allmatches.Interface_viewDetail;
import com.mytrinity.cricreport_plus.plan_page.Plans;
import com.mytrinity.cricreport_plus.pojo.MatchesDetail;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static android.content.ContentValues.TAG;
import static android.content.Context.MODE_PRIVATE;

public class AllMatchesFragment extends Fragment implements PaymentResultListener {

    View layout;
    Checkout checkout;
    private boolean firstTime_live = false;
    RecyclerView recyclerView_allmatches;
    ProgressBar progressBar;
    Adapter_allmatches adapter_allmatches;
    Button btnBack;
    TextView txtjTitle;
    int amount;
    private final List<MatchesDetail> list_upcoming = new ArrayList<>();
    private final List<MatchesDetail> list_recent = new ArrayList<>();
    private final List<MatchesDetail> list_live = new ArrayList<>();
    List<Object> data_allmatches = new ArrayList<>();
    private static ArrayList<String> matchkeys = new ArrayList<>();
    LinearLayout popuplayout;
    String match_id_hold;
    public static final String Default = "N/A";
    String uidHold,holdnm,holdmobile;
    FirebaseAuth mAuth;
    FirebaseUser user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_allmatches, container, false);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        Checkout.preload(getActivity().getApplicationContext());

        checkout = new Checkout();
        SharedPreferences prefs = getActivity().getSharedPreferences("userDetail", MODE_PRIVATE);
        uidHold = prefs.getString("uid", Default);
        holdnm = prefs.getString("name", Default);
        holdmobile = prefs.getString("mobile", Default);

        recyclerView_allmatches = layout.findViewById(R.id.recycler_allmatches);
        progressBar=layout.findViewById(R.id.progressbar);
        popuplayout=layout.findViewById(R.id.popuplayout);
        btnBack=layout.findViewById(R.id.btnBack);
        txtjTitle=layout.findViewById(R.id.txtjTitle);

        if(user!=null) {
            loadmatchKeys(uidHold);
        }
        else
        {
            load();
        }

        return layout;
    }

    private void load() {

        try {
            loadFeatured loader = new loadFeatured();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);
                progressBar.setVisibility(View.VISIBLE);
            }

        } catch (Exception ignored) {
        }

    }

    private class loadFeatured extends Thread {


        @Override
        public void run() {
//            progress.setVisibility(View.GONE);
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("Featured_match_predict/");

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    list_recent.clear();
                    list_live.clear();
                    list_upcoming.clear();

                    if (firstTime_live) {
                        for (DataSnapshot dataSnapshot1 : snapshot.getChildren()) {
                            MatchesDetail listdata = new MatchesDetail();

//                        listdata.team_1_logo = String.valueOf(dataSnapshot1.child("flag1").getValue());
//                        listdata.team_2_logo = String.valueOf(dataSnapshot1.child("flag2").getValue());
                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.plan_price = String.valueOf(dataSnapshot1.child("planPrice").getValue());
                            listdata.desc = String.valueOf(dataSnapshot1.child("planDesc").getValue());
//                        listdata.active = String.valueOf(dataSnapshot1.child("active").getValue());
                            listdata.match_no = "";

                            if (matchkeys.contains(String.valueOf(dataSnapshot1.getKey()))) {
                                listdata.joinSts = "join";
                            } else {
                                listdata.joinSts = "none";
                            }

                            if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("1")) {
                                list_live.add(listdata);
                            } else if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("0")) {

                                list_upcoming.add(listdata);
                            }

                        }

                        getData();
                        adapter_allmatches.notifyDataSetChanged();
                        progressBar.setVisibility(View.GONE);
                    }else
                    {
                        firstTime_live=true;
                        for (DataSnapshot dataSnapshot1 : snapshot.getChildren()) {
                            MatchesDetail listdata = new MatchesDetail();

//                        listdata.team_1_logo = String.valueOf(dataSnapshot1.child("flag1").getValue());
//                        listdata.team_2_logo = String.valueOf(dataSnapshot1.child("flag2").getValue());
                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.plan_price = String.valueOf(dataSnapshot1.child("planPrice").getValue());
                            listdata.desc = String.valueOf(dataSnapshot1.child("planDesc").getValue());
//                        listdata.active = String.valueOf(dataSnapshot1.child("active").getValue());
                            listdata.match_no = "";

                            if (matchkeys.contains(String.valueOf(dataSnapshot1.getKey()))) {
                                listdata.joinSts = "join";
                            } else {
                                listdata.joinSts = "none";
                            }

                            if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("1")) {
                                list_live.add(listdata);
                            } else if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("0")) {

                                list_upcoming.add(listdata);
                            }

                        }


                        callRecycle_allmatches();
                        progressBar.setVisibility(View.GONE);

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(getActivity(), ""+error, Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);

                }
            });


        }
    }

    public void callRecycle_allmatches() {
        adapter_allmatches = new Adapter_allmatches(getActivity(), getData());
        recyclerView_allmatches.setAdapter(adapter_allmatches);
        recyclerView_allmatches.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter_allmatches.onselectClick(new Interface_allmatches() {
            @Override
            public void onClick(int Position, String joinSts) {


            }

        }, new Interface_viewDetail() {
            @Override
            public void onViewClick(int Position, String desc) {
                txtjTitle.setText(desc);
                showPopup();
            }
        }, new Interface_JoinClick() {
            @Override
            public void onJoinClick(int Position, String planPrice,String JoinSts) {
                MatchesDetail current= (MatchesDetail) data_allmatches.get(Position);
                match_id_hold=current.match_id;

                if (JoinSts.equals("join")) {

                    Intent intent = new Intent(getActivity(), Plans.class);
                    intent.putExtra("price", planPrice);
                    intent.putExtra("match_id", match_id_hold);
                    startActivity(intent);
                } else {
                    if(user==null)
                    {
                        showLoginAlert();
                        return;
                    }
                    addCashClick(planPrice);

                }

            }
        });
    }

    public List<Object> getData() {

        data_allmatches.clear();
        for (int i = 0; i < list_live.size(); i++) {
            MatchesDetail info = new MatchesDetail();

            info = list_live.get(i);

            data_allmatches.add(info);
        }
        for (int i = 0; i < list_upcoming.size(); i++) {
            MatchesDetail info = new MatchesDetail();

            info = list_upcoming.get(i);

            data_allmatches.add(info);
        }

        return data_allmatches;
    }

    public void showPopup() {

        popuplayout.setVisibility(View.VISIBLE);

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())


                .setContentHolder(new ViewHolder(popuplayout))
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(true, ViewGroup.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.BOTTOM)
                .setMargin(0, 500, 0, 0)
//                .setInAnimation(R.anim.abc_fade_in)
//                .setOutAnimation(R.anim.abc_fade_out)


                .setContentBackgroundResource(R.color.colorPrimary_faint)// This will enable the expand feature, (similar to android L share dialog)
                .setCancelable(true)

                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialogPlus) {
                        popuplayout.setVisibility(View.GONE);
                    }
                })


                .create();

        dialog.show();


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public void onJoin_click(String planPrice) {

        progressBar.setVisibility(View.VISIBLE);


//        if (uidHold == Default) {
//            Toast.makeText(AllMatches.this, "You Are Not Login Yet, Login First", Toast.LENGTH_SHORT).show();
//            progressbar.setVisibility(View.GONE);
//        } else {



//            Toast.makeText(AllMatches.this, "pkey"+planKey, Toast.LENGTH_SHORT).show();
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference ref = database.getReference("/UserMatches" + "/" + uidHold);
            Date c = Calendar.getInstance().getTime();

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
            String formattedDate = df.format(c);

            HashMap detail = new HashMap();
            detail.put("match_id", match_id_hold);
            detail.put("user_id", uidHold);
            detail.put("date", formattedDate);
            detail.put("planPrice", planPrice);

            ref.child(match_id_hold).setValue(detail).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    progressBar.setVisibility(View.GONE);
                    if (task.isComplete()) {
                        Toast.makeText(getActivity(), "Successfully Join", Toast.LENGTH_SHORT).show();

//                        loadmatchKeys(plan_id);
//
//                        Intent intent = new Intent(AllMatches.this, Plans.class);
//                        intent.putExtra("price", price);
//                        intent.putExtra("planKey", plan_id);
//                        intent.putExtra("match_id", match_id_hold);
//                        startActivity(intent);
//                        finish();


                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getActivity(), "Failed Join", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            });

//        }
//       startActivity(new Intent(Contest_detail.this,JoinContest.class));
    }

    private void loadmatchKeys(String userid) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("/UserMatches/" + userid);

        ref.addListenerForSingleValueEvent(new com.google.firebase.database.ValueEventListener() {
            @Override
            public void onDataChange(@NonNull com.google.firebase.database.DataSnapshot snapshot) {
                matchkeys.clear();
                for (com.google.firebase.database.DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    Boolean user_id = dataSnapshot.child("user_id").exists();
                    if (user_id == true) {
                        matchkeys.add(dataSnapshot.getKey());
                    }
                }

//                if(plankeys.size()>0) {
                load();
//                callContinousForTime();
                progressBar.setVisibility(View.VISIBLE);
//                }
//                else
//                {
//                    progressbar_plan.setVisibility(View.GONE);
//                }
//                callContinousForTime();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }


    public void addCashClick(final String planPrice) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            Toast.makeText(getActivity(), "yes click", Toast.LENGTH_SHORT).show();
                            startPayment(planPrice);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            Toast.makeText(getActivity(), "no click", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Are you sure? You want to Buy this plan").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


    }

    public void startPayment(String planPrice) {
        checkout.setKeyID("rzp_test_iPey9742zxKNZA");
//        rzp_test_iPey9742zxKNZA  TEST KEY
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = getActivity();

        amount= Integer.parseInt(planPrice)*100;

        /**
         * Set your logo here
         */
        checkout.setImage(R.mipmap.ic_launcher);

        try {
            JSONObject options = new JSONObject();

            options.put("name", "akshay");
            options.put("description", "Buying Plan");
            options.put("image", "http://crickhabar.in/images/logo.png");
//            options.put("order_id", "order_DBJOWzybf0sJbb");//from response of step 3.
            options.put("theme.color", "#111d5e");
            options.put("currency", "INR");
            options.put("amount", amount);//pass amount in currency subunits

            String mail="akshay"+"@gmail.com";
            options.put("prefill.email", mail);
            options.put("prefill.contact","9890433938");
            checkout.open(activity, options);
        } catch(Exception e) {
            Log.e(TAG, "Error in starting Razorpay Checkout", e);
        }
    }

    @Override
    public void onPaymentSuccess(final String response) {

        Toast.makeText(getActivity(), ""+response, Toast.LENGTH_SHORT).show();

        HashMap data=new HashMap();
        data.put("amt",amount);
        data.put("message"," ");
        data.put("status","CREDIT");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd HH:mm", Locale.getDefault());
        String currentDateandTime = sdf.format(new Date());
        data.put("date",currentDateandTime);
        data.put("response",response);

        FirebaseDatabase.getInstance().getReference("wallet/history/"+uidHold).push().setValue(data);

        onJoin_click(String.valueOf(amount));

        Intent intent=new Intent(getActivity(), MainActivity.class);
        intent.putExtra("amount",amount);
        intent.putExtra("status","true");
        startActivity(intent);

    }

    @Override
    public void onPaymentError(int i, String s) {

        Toast.makeText(getActivity(), ""+s, Toast.LENGTH_LONG).show();
        Toast.makeText(getActivity(), "Something went wrong try again", Toast.LENGTH_LONG).show();

    }

    public void showLoginAlert() {

        Snackbar.make(layout, "Login First To Join Match", Snackbar.LENGTH_LONG)
                .setAction("LOGIN", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        startActivity(new Intent(getActivity(), Login.class));
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light ))
                .setBackgroundTint(getResources().getColor(R.color.odds_1_color))
                .setTextColor(getResources().getColor(R.color.white))
                .show();

    }


}