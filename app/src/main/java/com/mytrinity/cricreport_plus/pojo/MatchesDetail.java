package com.mytrinity.cricreport_plus.pojo;

import java.io.Serializable;

public class MatchesDetail implements Serializable {

    public int id;
    public String target;
    public String match_no;
    public String extra_text;
    public String live;
    public String series;
    public String team_1;
    public String team_2;
    public String nickname_1;
    public String nickname_2;
    public String team_1_logo;
    public String team_1_score;
    public String team_1_overs;
    public String team_2_overs;
    public String team_2_logo;
    public String team_2_score;
    public String date_time;
    public String match_id;
    public String fav;
    public String rate_1;
    public String rate_2;
    public String active;
    public String joinSts;
    public int type;
    public String plan_price;
    public String desc;

}

