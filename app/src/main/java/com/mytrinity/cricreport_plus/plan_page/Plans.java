package com.mytrinity.cricreport_plus.plan_page;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mytrinity.cricreport_plus.MainActivity;
import com.mytrinity.cricreport_plus.R;


import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.mytrinity.cricreport_plus.helper.LinkHolder.mamaLINK_4;


public class Plans extends AppCompatActivity {

    Toolbar toolbar;
    String price, match_id;
    TextView txtprice, txtdetail;
    private Firebase ref_1;
    LinearLayout team_layout;

    CircleImageView team_1_logo, team_2_logo;
    TextView team_1, team_2, team_1_score, team_2_score, team_1_over, team_2_over, extra_bottom;

    private String strike_txt, balls_done_txt, live_line_txt, last_balls_txt, need_runs_txt, batsmen_1_runs_txt, batsmen_1_balls_txt, batsmen_1_4s_txt, batsmen_1_6s_txt, batsmen_2_runs_txt, batsmen_2_balls_txt, batsmen_2_4s_txt, batsmen_2_6s_txt, remaining_balls_txt, runs_txt, wickets_txt;
    private String team_txt, power_play_txt, bowler_txt, batsmen_1_txt, batsmen_2_txt, story_board_txt, innings_txt, last_wicket_txtt, session_overs_txt, favorite_txt, favorite_2_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        toolbar = findViewById(R.id.app_bar);

        Firebase.setAndroidContext(this);

        txtprice = findViewById(R.id.txtprice);
        txtdetail = findViewById(R.id.txtdetail);

        team_1_logo = findViewById(R.id.team_1_logo);
        team_2_logo = findViewById(R.id.team_2_logo);
        team_1 = findViewById(R.id.team_1);
        team_2 = findViewById(R.id.team_2);
        team_1_score = findViewById(R.id.team_1_score);
        team_2_score = findViewById(R.id.team_2_score);
        team_1_over = findViewById(R.id.team_1_over);
        team_2_over = findViewById(R.id.team_2_over);
        extra_bottom = findViewById(R.id.extra_bottom);

        team_layout = findViewById(R.id.team_layout);


        Intent i = getIntent();
        price = i.getStringExtra("price");
        match_id = i.getStringExtra("match_id");

        ref_1 = new Firebase(mamaLINK_4.replace("{mamaID}", match_id));
        if (price != null) {
            txtprice.setText("Selected Plan Price : " + getResources().getString(R.string.Rs) + price);
        }

        try {
            getValue();
        } catch (Exception e1) {
            txtdetail.setText("Not Added Yet");
        }

        loadScore();

    }

    public void getValue() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Plans_detail_predict/" + match_id);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                String detail = "";
                if (!(snapshot.child("detail").getValue() == null)) {
                    detail = snapshot.child("detail").getValue().toString();
                }

                txtdetail.setText(detail);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {


            }
        });
    }


    private class loadLine extends Thread {

        @Override
        public void run() {

            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            ref_1.addValueEventListener(new com.firebase.client.ValueEventListener() {
                @Override
                public void onDataChange(@NonNull com.firebase.client.DataSnapshot dataSnapshot) {

                    try {


//                        ref2

                        strike_txt = String.valueOf(dataSnapshot.child("ap").getValue());
                        balls_done_txt = String.valueOf(dataSnapshot.child("bd").getValue());
                        live_line_txt = String.valueOf(dataSnapshot.child("cb").getValue());
                        last_balls_txt = String.valueOf(dataSnapshot.child("lb").getValue());
                        need_runs_txt = String.valueOf(dataSnapshot.child("nr").getValue());
                        batsmen_1_runs_txt = String.valueOf(dataSnapshot.child("p1r").getValue());
                        batsmen_1_balls_txt = String.valueOf(dataSnapshot.child("p1b").getValue());
                        batsmen_1_4s_txt = String.valueOf(dataSnapshot.child("p14").getValue());
                        batsmen_1_6s_txt = String.valueOf(dataSnapshot.child("p16").getValue());
                        batsmen_2_runs_txt = String.valueOf(dataSnapshot.child("p2r").getValue());
                        batsmen_2_balls_txt = String.valueOf(dataSnapshot.child("p2b").getValue());
                        batsmen_2_4s_txt = String.valueOf(dataSnapshot.child("p24").getValue());
                        batsmen_2_6s_txt = String.valueOf(dataSnapshot.child("p26").getValue());
                        remaining_balls_txt = String.valueOf(dataSnapshot.child("rb").getValue());
                        runs_txt = String.valueOf(dataSnapshot.child("s").getValue());
                        wickets_txt = String.valueOf(dataSnapshot.child("w").getValue());

//                        ref3

                        team_txt = String.valueOf(dataSnapshot.child("st").getValue());
                        power_play_txt = String.valueOf(dataSnapshot.child("pp").getValue());
                        bowler_txt = String.valueOf(dataSnapshot.child("b").getValue());
                        batsmen_1_txt = String.valueOf(dataSnapshot.child("p1").getValue());
                        batsmen_2_txt = String.valueOf(dataSnapshot.child("p2").getValue());
                        story_board_txt = String.valueOf(dataSnapshot.child("c2").getValue());
                        innings_txt = String.valueOf(dataSnapshot.child("i").getValue());
                        last_wicket_txtt = String.valueOf(dataSnapshot.child("lw").getValue());
                        session_overs_txt = String.valueOf(dataSnapshot.child("so").getValue());
                        favorite_txt = String.valueOf(dataSnapshot.child("rt").getValue());
                        favorite_2_txt = String.valueOf(dataSnapshot.child("rt2").getValue());

//


//                        loadAtLast();

                    } catch (Exception ignored) {
                    }

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });


        }

    }

//    private void loadAtLast() {
//
//
//        try {
//
//
//
//            name_score1.setText(team_txt);
//
//            scores1.setText(runs_txt+"/"+wickets_txt);
//
//            int totalballs = Integer.parseInt(balls_done_txt);
//            String overss = (totalballs / 6) + "." + (totalballs % 6);
//            overs1.setText( "  " +overss +" overs" );
//
//
//
//            try {
//
//                crr.setText("" + Double.valueOf(((double) Math.round(Double.valueOf(Double.parseDouble(runs_txt) / (((double) totalballs) / 6.0d)).doubleValue() * 100.0d)) / 100.0d));
//                crr_txt.setVisibility(View.VISIBLE);
//                crr.setVisibility(View.VISIBLE);
//
//            } catch (Exception e) {
//                crr_txt.setVisibility(View.GONE);
//                crr.setVisibility(View.GONE);
//            }
//
//            try {
//
//                if (innings_txt.equals("1") || type == 1) {
//                    needed_runs_balls.setVisibility(View.GONE);
//                } else {
//                    if (livee.matches("1")) {
//                        needed_runs_balls.setVisibility(View.VISIBLE);
//                        needed_runs_balls.setText("Need " + need_runs_txt + " runs on " + remaining_balls_txt + " balls.");
//                        rrr.setVisibility(View.VISIBLE);
//                        rrr_txt.setVisibility(View.VISIBLE);
//                        rrr.setText("" + Double.valueOf(((double) Math.round(Double.valueOf(Double.parseDouble(need_runs_txt) / (Double.parseDouble(remaining_balls_txt) / 6.0d)).doubleValue() * 100.0d)) / 100.0d));
//                    } else {
//                        needed_runs_balls.setVisibility(View.GONE);
//                        rrr.setVisibility(View.GONE);
//                        rrr_txt.setVisibility(View.GONE);
//                    }
//                }
//
//            } catch (Exception e) {
//                needed_runs_balls.setVisibility(View.GONE);
//                rrr.setVisibility(View.GONE);
//                rrr_txt.setVisibility(View.GONE);
//            }
//
//            bowler_name.setText(bowler_txt);
//
//            if (strike_txt.matches("p1")) {
//
//                batsmen_1.setText(batsmen_1_txt);
//                batsmen_2.setText(batsmen_2_txt);
//
//                batsmen_1_4s.setText(batsmen_1_4s_txt);
//                batsmen_1_6s.setText(batsmen_1_6s_txt);
//                batsmen_1_ball.setText(batsmen_1_balls_txt);
//                batsmen_1_run.setText(batsmen_1_runs_txt);
//
//                batsmen_2_4s.setText(batsmen_2_4s_txt);
//                batsmen_2_6s.setText(batsmen_2_6s_txt);
//                batsmen_2_ball.setText(batsmen_2_balls_txt);
//                batsmen_2_run.setText(batsmen_2_runs_txt);
//
//                try {
//
//                    String calc = String.format(Locale.getDefault(), "%.2f", (Float.parseFloat(batsmen_1_runs_txt) / Float.parseFloat(batsmen_1_balls_txt)) * 100);
//
//                    if (calc.matches("NaN")) {
//                        batsmen_1_sr.setText("0.0");
//                    } else {
//                        batsmen_1_sr.setText(calc);
//                    }
//
//                } catch (Exception e) {
//                    batsmen_1_sr.setText("0.0");
//                }
//
//                try {
//
//                    String calc = String.format(Locale.getDefault(), "%.2f", (Float.parseFloat(batsmen_2_runs_txt) / Float.parseFloat(batsmen_2_balls_txt)) * 100);
//
//                    if (calc.matches("NaN")) {
//                        batsmen_2_sr.setText("0.0");
//                    } else {
//                        batsmen_2_sr.setText(calc);
//                    }
//
//                } catch (Exception e) {
//                    batsmen_2_sr.setText("0.0");
//                }
//
//            } else {
//
//                batsmen_1.setText(batsmen_2_txt);
//                batsmen_2.setText(batsmen_1_txt);
//
//                batsmen_2_4s.setText(batsmen_1_4s_txt);
//                batsmen_2_6s.setText(batsmen_1_6s_txt);
//                batsmen_2_ball.setText(batsmen_1_balls_txt);
//                batsmen_2_run.setText(batsmen_1_runs_txt);
//
//                batsmen_1_4s.setText(batsmen_2_4s_txt);
//                batsmen_1_6s.setText(batsmen_2_6s_txt);
//                batsmen_1_ball.setText(batsmen_2_balls_txt);
//                batsmen_1_run.setText(batsmen_2_runs_txt);
//
//                try {
//
//                    String calc = String.format(Locale.getDefault(), "%.2f", (Float.parseFloat(batsmen_2_runs_txt) / Float.parseFloat(batsmen_2_balls_txt)) * 100);
//
//                    if (calc.matches("NaN")) {
//                        batsmen_1_sr.setText("0.0");
//                    } else {
//                        batsmen_1_sr.setText(calc);
//                    }
//
//                } catch (Exception e) {
//                    batsmen_1_sr.setText("0.0");
//                }
//
//                try {
//
//                    String calc = String.format(Locale.getDefault(), "%.2f", (Float.parseFloat(batsmen_1_runs_txt) / Float.parseFloat(batsmen_1_balls_txt)) * 100);
//
//                    if (calc.matches("NaN")) {
//                        batsmen_2_sr.setText("0.0");
//                    } else {
//                        batsmen_2_sr.setText(calc);
//                    }
//
//                } catch (Exception e) {
//                    batsmen_2_sr.setText("0.0");
//                }
//
//            }
//
//            l6b.setText(last_balls_txt);
//
//            if (last_wicket_txtt.matches("")) {
//                last_wicket_txt.setVisibility(View.GONE);
//                last_wicket.setVisibility(View.GONE);
//            } else {
//                last_wicket_txt.setVisibility(View.VISIBLE);
//                last_wicket.setVisibility(View.VISIBLE);
//                last_wicket.setText(last_wicket_txtt);
//            }
//
//
//
//        } catch (Exception e) {
//            //Toast.makeText(getActivity(), "Error encountered, Please check in some time.", Toast.LENGTH_SHORT).show();
//        }
//
//    }


    private void load(String mid) {

        try {

            loadFeatured loader = new loadFeatured(mid);
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);
            }

        } catch (Exception ignored) {
        }

    }


    private class loadFeatured extends Thread {

        String mid;

        public loadFeatured(String mid) {
            this.mid = mid;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            final Firebase ref = new Firebase("https://cricket-exchange-6.firebaseio.com/pliveMatches2/" + mid);

            ref.addValueEventListener(new com.firebase.client.ValueEventListener() {
                @Override
                public void onDataChange(@NonNull com.firebase.client.DataSnapshot dataSnapshot) {


                    String team1_ovr = String.valueOf(dataSnapshot.child("j").getValue());

                    String team2_ovr = String.valueOf(dataSnapshot.child("k").getValue());

                    String team1_run = "0/0", team1_ov = "", team2_run = "0/0", team2_ov = "";
                    String s = team1_ovr;
                    try {
                        String[] split = s.split(Pattern.quote("("));
                        team1_run = split[0];
                        team1_ov = split[1];
                    } catch (Exception e1) {
                        team1_run = "0/0";
                    }

                    String ss = team2_ovr;
                    try {
                        String[] splits = ss.split(Pattern.quote("("));
                        team2_run = splits[0];
                        team2_ov = splits[1];
                    } catch (Exception e1) {
                        team2_run = "0/0";
                    }


                    team_1_over.setText(team1_ov + " ovr");
                    team_2_over.setText(team2_ov + " ovr");
                    team_1_score.setText(team1_run);
                    team_2_score.setText(team2_run);

                    String extra = String.valueOf(dataSnapshot.child("a").getValue());

                    if (extra.length() > 15) {
                        extra_bottom.setText(extra);
                        extra_bottom.setVisibility(View.VISIBLE);
                    } else {

                        extra_bottom.setVisibility(View.GONE);
//                            listdata.extra_text = "";
                    }


                    //ref.removeEventListener(this);

                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                }
            });


        }
    }

    private void load_match() {

        try {
            loadMatch loader = new loadMatch();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);

            }

        } catch (Exception ignored) {
        }

    }

    private class loadMatch extends Thread {


        @Override
        public void run() {
//            progress.setVisibility(View.GONE);
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("Featured_match_predict/" + match_id);

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    team_1.setText(snapshot.child("team1").getValue().toString());
                    team_2.setText(snapshot.child("team2").getValue().toString());

                    String tm1_url = snapshot.child("flag1").getValue().toString();
                    String tm2_url = snapshot.child("flag2").getValue().toString();

                    Glide.with(Plans.this).load(tm1_url).placeholder(R.drawable.criclogo).into(team_1_logo);
                    Glide.with(Plans.this).load(tm2_url).placeholder(R.drawable.criclogo).into(team_2_logo);


                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });

        }
    }

    public void loadScore() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference ref = database.getReference("Livescore/" + match_id);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {


                if (snapshot.child("mamaID").getValue() == null) {
                    team_layout.setVisibility(View.GONE);
                } else {
                    String mid = snapshot.child("mamaID").getValue().toString();
                    load_match();
                    load(mid);
                    team_layout.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }


}