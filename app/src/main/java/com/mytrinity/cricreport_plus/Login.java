package com.mytrinity.cricreport_plus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.FirebaseDatabase;
import com.mytrinity.cricreport_plus.helper.hide_Keyboard;


import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Login extends AppCompatActivity {

    private FirebaseAuth mAuth;
    final static int RC_SIGN_IN = 989;
    private GoogleSignInClient mGoogleSignInClient;


    CardView btnSignWithGoogle;

    String mVerificationId;

    EditText edtphoneNumber, edtname, edtmail;
    ProgressBar progressbar;
    String otpHold = null;
    PinEntryEditText pinEntry;

    LinearLayout profiledetailCard, maindetailCard;


    LinearLayout phonenumberLayout, verifyLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();

        profiledetailCard = findViewById(R.id.profiledetailCard);
        maindetailCard = findViewById(R.id.maindetailCard);
        edtname = findViewById(R.id.edtname);
        edtmail = findViewById(R.id.edtemail);


        edtphoneNumber = findViewById(R.id.edtphoneNumber);

        phonenumberLayout = findViewById(R.id.phonenumberLayout);
        verifyLayout = findViewById(R.id.verifyLayout);
        verifyLayout.setVisibility(View.GONE);

        progressbar = findViewById(R.id.progressbar);

        btnSignWithGoogle = findViewById(R.id.signWithGoogle);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        btnSignWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });


        pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry);


        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().length() > 5) {
                        otpHold = str.toString();
                        new hide_Keyboard(Login.this);

                        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otpHold);

                        signInWithPhoneAuthCredential(credential);

                    } else {
                        Toast.makeText(Login.this, "FAIL", Toast.LENGTH_SHORT).show();
                        pinEntry.setText(null);
                    }
                }
            });
        }


    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
//
//                            Log.d(TAG, "signInWithCredential:success");
                            Toast.makeText(Login.this, "signInWithCredential:success", Toast.LENGTH_SHORT).show();
//                            FirebaseUser user = mAuth.getCurrentUser();
//                            updateUI(user);
                            saveUser();
                        } else {
                            // If sign in fails, display a message to the user.
//                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Login.this, "signInWithCredential:failure", Toast.LENGTH_SHORT).show();
//                            Snackbar.make(mBinding.mainLayout, "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();

//        mAuth.getInstance().signOut();
//         Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(Login.this, MainActivity.class));
        }
//        updateUI(currentUser);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
//                Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());


            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
//                Log.w(TAG, "Google sign in failed", e);
                // ...
                Toast.makeText(this, "google faied" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


//    phone Verification

    public void loginClick(View v) {

        int ncount = edtphoneNumber.getText().toString().length();
        if (edtphoneNumber.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
        } else if (ncount<10) {
            Toast.makeText(this, "Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
        }else if (edtname.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Your Name", Toast.LENGTH_SHORT).show();
        }
        else {
            phonenumberLayout.setVisibility(View.GONE);
            verifyLayout.setVisibility(View.VISIBLE);

            sendVerificationCode();
        }

    }

    public void sendVerificationCode() {
        progressbar.setVisibility(View.VISIBLE);
        String phoneNumber = "+91" + edtphoneNumber.getText().toString();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.
//            Log.d(TAG, "onVerificationCompleted:" + credential);

//            Toast.makeText(Login.this, "onVerificationCompleted:" + credential, Toast.LENGTH_SHORT).show();

            signInWithPhoneAuthCredential(credential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            // This callback is invoked in an invalid request for verification is made,
            // for instance if the the phone number format is not valid.
//            Log.w(TAG, "onVerificationFailed", e);

            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                // Invalid request
                // ...
                Toast.makeText(Login.this, "Invalid request", Toast.LENGTH_SHORT).show();
                progressbar.setVisibility(View.GONE);
            } else if (e instanceof FirebaseTooManyRequestsException) {
                // The SMS quota for the project has been exceeded
                // ...

                Toast.makeText(Login.this, "he SMS quota for the project has been exceeded", Toast.LENGTH_SHORT).show();
                progressbar.setVisibility(View.GONE);
            }

            // Show a message and update the UI
            // ...
        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.
//            Log.d(TAG, "onCodeSent:" + verificationId);

//            Toast.makeText(Login.this, "onCodeSent:" + verificationId, Toast.LENGTH_SHORT).show();

            // Save verification ID and resending token so we can use them later
            mVerificationId = verificationId;
//            mResendToken = token;

            // ...
        }
    };

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        progressbar.setVisibility(View.VISIBLE);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
//                            Log.d(TAG, "signInWithCredential:success");

                            progressbar.setVisibility(View.GONE);

                            Toast.makeText(Login.this, "signInWithCredential:success", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = task.getResult().getUser();
                            // ...
                            edtphoneNumber.setText("");
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            SharedPreferences.Editor editor = getSharedPreferences("userDetail", MODE_PRIVATE).edit();
                            editor.putString("uid",currentUser.getUid());
                            editor.putString("name",edtname.getText().toString());
                            editor.putString("mobile",currentUser.getPhoneNumber());
                            editor.apply();

                            saveUser();
                            edtname.setText("");
                            startActivity(new Intent(Login.this, MainActivity.class));

                        } else {
                            // Sign in failed, display a message and update the UI
//                            Log.w(TAG, "signInWithCredential:failure", task.getException());

                            progressbar.setVisibility(View.GONE);
                            Toast.makeText(Login.this, "signInWithCredential:failure", Toast.LENGTH_SHORT).show();

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }


//    save Detail

    public void saveUser() {
//        startActivity(new Intent(Login.this, MainActivity.class));
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            HashMap<String, Object> userDetail = new HashMap<String, Object>();

            if (!(edtname.getText().equals(""))) {
                userDetail.put("name", edtname.getText().toString());
            }
            if (currentUser.getPhoneNumber() != null || currentUser.getPhoneNumber() == "") {
                userDetail.put("mobile", currentUser.getPhoneNumber());
            }
            if (currentUser.getEmail() != null || currentUser.getEmail() == "") {
                userDetail.put("email", currentUser.getEmail());
            }
            FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid())
                    .updateChildren(userDetail)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
//                            Toast.makeText(Login.this, "Su", Toast.LENGTH_SHORT).show();

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(Login.this, "Something went wrong please try again " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(Login.this, "Unale to find user try again", Toast.LENGTH_SHORT).show();
        }

    }

    public void detailClick(View v) {
        if (edtname.getText().toString().isEmpty()) {
            Toast.makeText(this, "enter Name", Toast.LENGTH_SHORT).show();
        }
        else  if (edtmail.getText().toString().isEmpty()) {
            Toast.makeText(this, "enter Mail Id", Toast.LENGTH_SHORT).show();
        }
        else {
            HashMap detail = new HashMap();
            detail.put("name", edtname.getText().toString());
            detail.put("email", edtmail.getText().toString());
            FirebaseUser currentUser = mAuth.getCurrentUser();
            FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid())
                    .updateChildren(detail);


        }
    }

    public void verifyOtp(View v)
    {

        if(otpHold.length()>5)
        {
            new hide_Keyboard(Login.this);

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, otpHold);

            signInWithPhoneAuthCredential(credential);
        }
        else
        {
            Toast.makeText(this, "Enter Valid Otp", Toast.LENGTH_SHORT).show();
        }

    }

}
