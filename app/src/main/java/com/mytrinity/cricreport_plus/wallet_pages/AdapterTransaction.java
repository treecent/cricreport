package com.mytrinity.cricreport_plus.wallet_pages;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.mytrinity.cricreport_plus.R;
import com.mytrinity.cricreport_plus.pojo.TransactionDetail;

import java.util.ArrayList;
import java.util.List;

public class AdapterTransaction extends RecyclerView.Adapter<AdapterTransaction.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;

    List<Object> data = new ArrayList<>();


    public AdapterTransaction(Context context, List<Object> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;

    }



    @Override
    public AdapterTransaction.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_transaction, parent, false);
        AdapterTransaction.MyViewHolder viewHolder = new AdapterTransaction.MyViewHolder(view);
        return viewHolder;


    }


    @Override
    public void onBindViewHolder(final AdapterTransaction.MyViewHolder holder, int position) {

        TransactionDetail current = (TransactionDetail) data.get(position);

        holder.txtAmt.setText(context.getResources().getString(R.string.Rs)+current.getAmt());
        holder.txtMessage.setText(current.getMessage());
        holder.txtsts.setText(current.getSts());

        holder.date.setText(current.getDate());
        holder.id.setText("Pid :"+current.getId());

        if(current.getSts().equals("CREDIT"))
        {
            holder.txtsts.setTextColor(context.getResources().getColor(R.color.green1));
        }
        else
        {
            holder.txtsts.setTextColor(context.getResources().getColor(R.color.red));
        }


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                context.startActivity(new Intent(context, Contest.class));
            }
        });


    }


    @Override
    public int getItemCount() {

        return data.size();
    }



    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        ImageView imgSlider;
        TextView txtAmt,txtMessage,txtsts,date,id;

        public MyViewHolder(View itemView) {
            super(itemView);

//            imgSlider=itemView.findViewById(R.id.imgSlider);
            mainLayout=itemView.findViewById(R.id.mainLayout);
            txtAmt=itemView.findViewById(R.id.txtAmt);
            txtMessage=itemView.findViewById(R.id.txtMessage);
            txtsts=itemView.findViewById(R.id.txtsts);
            date=itemView.findViewById(R.id.date);
            id=itemView.findViewById(R.id.id);



        }


    }


}

