package com.mytrinity.cricreport_plus.wallet_pages;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mytrinity.cricreport_plus.R;
import com.mytrinity.cricreport_plus.pojo.TransactionDetail;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class RecentTransaction extends AppCompatActivity {

    Toolbar toolbar;
    TextView toolTitle,toolbar_subtitle;
    RecyclerView recyclerView_transaction;
    AdapterTransaction adapterTransaction;
    private static List<TransactionDetail> listTransaction = new ArrayList<>();
    ImageView imgpdf;


    public static final String Default = "N/A";
    String uidHold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_transaction);

        SharedPreferences prefs = getSharedPreferences("userDetail", MODE_PRIVATE);
        uidHold = prefs.getString("uid", Default);

        toolbar = findViewById(R.id.app_bar);
        toolTitle = findViewById(R.id.toolbar_title);
        toolbar_subtitle= findViewById(R.id.toolbar_subtitle);
        toolTitle.setText("Recent Transaction");
        toolbar_subtitle.setText("");

        recyclerView_transaction = findViewById(R.id.recycler_transaction);
        imgpdf = findViewById(R.id.imgpdf);


        if(uidHold!=Default) {
            load();
        }
    }

    public void callTransaction() {
        adapterTransaction = new AdapterTransaction(RecentTransaction.this, getTransactionDetail());
        recyclerView_transaction.setAdapter(adapterTransaction);
        recyclerView_transaction.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

    }


    public List<Object> getTransactionDetail() {
        List<Object> data = new ArrayList<>();

        for (int i = 0; i < listTransaction.size(); i++) {
            data.add(listTransaction.get(i));

        }

        return data;
    }

    public void load() {
        try {

           LoadTransaction loader = new LoadTransaction();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();

                    executor.execute(loader);
            }

        } catch (Exception ignored) {
        }

    }

    private class LoadTransaction extends Thread {

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            DatabaseReference ref= FirebaseDatabase.getInstance().getReference("wallet/history/"+uidHold);

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {

                    listTransaction.clear();
                    for(DataSnapshot dataSnapshot:snapshot.getChildren())
                    {
                        TransactionDetail current=new TransactionDetail();
                        current.setAmt(dataSnapshot.child("amt").getValue().toString());
                        current.setMessage(dataSnapshot.child("message").getValue().toString());
                        current.setDate(dataSnapshot.child("date").getValue().toString());
                        current.setSts(dataSnapshot.child("status").getValue().toString());
                        current.setId(dataSnapshot.child("response").getValue().toString());

                        listTransaction.add(current);

                    }

                    if(listTransaction.size()>0)
                    {
                        Collections.reverse(listTransaction);
                        callTransaction();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(RecentTransaction.this, ""+error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
