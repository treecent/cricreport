package com.mytrinity.cricreport_plus.fragments_mymatches.list_recent;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mytrinity.cricreport_plus.R;
import com.mytrinity.cricreport_plus.main_fragment.list_allmatches.Interface_viewDetail;
import com.mytrinity.cricreport_plus.plan_page.Plans;
import com.mytrinity.cricreport_plus.pojo.MatchesDetail;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Adapter_recent_mymatch extends RecyclerView.Adapter<Adapter_recent_mymatch.MyViewHolder> {
    private LayoutInflater inflater;
    private static Context context;
    public Interface_recent_my interface_recent_my;
    public Interface_viewDetail interface_viewDetail;
    List<Object> data = new ArrayList<>();

    private ProgressDialog dialog;

    public Adapter_recent_mymatch(Context context, List<Object> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;

    }
    public void onselectClick(Interface_recent_my interface_recent_my,Interface_viewDetail interface_viewDetail)
    {
        this.interface_recent_my=interface_recent_my;
        this.interface_viewDetail=interface_viewDetail;
    }

    @Override
    public Adapter_recent_mymatch.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_upcoming_my, parent, false);
        Adapter_recent_mymatch.MyViewHolder viewHolder = new Adapter_recent_mymatch.MyViewHolder(view);
        return viewHolder;


    }


    @Override
    public void onBindViewHolder(final Adapter_recent_mymatch.MyViewHolder holder, int position) {

        final MatchesDetail current = (MatchesDetail) data.get(position);

        holder.cardjoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Plans.class);
                intent.putExtra("price", current.plan_price);
                intent.putExtra("match_id", current.match_id);
                context.startActivity(intent);
//                interface_upcoming.onClick(current.match_id,holder.getAdapterPosition());
            }
        });

        holder.txtviewDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MatchesDetail cdata = (MatchesDetail) data.get(holder.getAdapterPosition());
                interface_viewDetail.onViewClick(holder.getAdapterPosition(),cdata.desc);
            }
        });

        holder.txtTeam1ShortName.setText(current.nickname_1);
        holder.txtTeam2ShortName.setText(current.nickname_2);
        holder.txtdate.setText(current.date_time);
        holder.txtplan.setText(context.getResources().getString(R.string.Rs)+" "+current.plan_price);

        if(current.joinSts.equals("join"))
        {
            holder.txtbuy.setText("Joined");
            holder.cardjoin.setCardBackgroundColor(ContextCompat.getColor(context,R.color.red));
        }

//        if(current.active==null || current.active.equals("null"))
//        {
//
//        }
//        else
//        {
//            holder.txtactive.setText(current.active+" users joined");
//            holder.txtactive.setVisibility(View.VISIBLE);
//        }


//        callTime(holder, current.date_time);
    }

    public void callTime(Adapter_recent_mymatch.MyViewHolder holder, String cdate) {

        // Custom date format
        SimpleDateFormat format = new SimpleDateFormat("d MMM, h:mm a");
        // Custom date format
        SimpleDateFormat format_d1 = new SimpleDateFormat("EEE MMM d HH:mm:ss");

        Date currentDate = new Date();
        String dateStop = cdate;
        Date d1 = null;
        Date d2 = null;
        try {
            d2 = format.parse(dateStop);
            d1 = format_d1.parse(String.valueOf(currentDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // Get msec from each, and subtract.
        long diff = d2.getTime() - d1.getTime();

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        long diffDay = diffHours / (24);
        long remainHour = diffHours % (24);
        String Hour = "", Min = "", Sec = "", Day = "";


        if (diffHours > 0) {
            if (remainHour > 0) {
                Hour = remainHour + "h";
            } else {
                Hour = diffHours + "h";
            }

        }
        if (diffMinutes > 0) {
            Min = diffMinutes + "m";
        }
        if (diffSeconds > 0) {
            Sec = diffSeconds + "s";
        }
        if (diffDay > 0) {
            Day = diffDay + "d";
        }
        holder.txtRemainTime.setText(Day + " " + Hour + " " + Min);

//        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {

        return data.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mainLayout;
        CardView cardjoin;
        TextView txtTeam1ShortName, txtTeam2ShortName, txtRemainTime, txtSeriesName,txtplan,txtbuy,txtviewDetail,txtdate;

        public MyViewHolder(View itemView) {
            super(itemView);

            mainLayout = itemView.findViewById(R.id.mainLayout);

            txtTeam1ShortName = itemView.findViewById(R.id.txtteam1);
            txtTeam2ShortName = itemView.findViewById(R.id.txtteam2);
            txtviewDetail= itemView.findViewById(R.id.txtviewDetail);
            txtdate= itemView.findViewById(R.id.txtdate);
            cardjoin= itemView.findViewById(R.id.cardjoin);

//            txtSeriesName = itemView.findViewById(R.id.txtSeriesName);
//            txtRemainTime = itemView.findViewById(R.id.txtRemainTime);
//            txtactive= itemView.findViewById(R.id.txtactive);

            txtplan= itemView.findViewById(R.id.txtplanprice);
            txtbuy= itemView.findViewById(R.id.txtbuy);


        }


    }


}

