package com.mytrinity.cricreport_plus.fragments_mymatches;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mytrinity.cricreport_plus.R;
import com.mytrinity.cricreport_plus.fragments_mymatches.list_liveMatches.Adapter_liveMatches;
import com.mytrinity.cricreport_plus.fragments_mymatches.list_liveMatches.Interface_live_my;
import com.mytrinity.cricreport_plus.main_fragment.list_allmatches.Interface_viewDetail;
import com.mytrinity.cricreport_plus.pojo.MatchesDetail;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static android.content.Context.MODE_PRIVATE;


public class LiveMatches extends Fragment implements View.OnClickListener {


    View layout;

    Adapter_liveMatches adapter_live;
    RecyclerView recyclerView_live;
    TextView txtNoLive;
    SwipeRefreshLayout swipelayout;
    ProgressBar progressBar,progressbar_plan;
    private boolean firstTime_live = false;
    private final List<MatchesDetail> list_live = new ArrayList<>();
    private static ArrayList<String> listkey = new ArrayList<>();
    private static ArrayList<String> plankeys = new ArrayList<>();


    public static String match_id_hold;
    private boolean clicked;
    LinearLayout popuplayout;
    Button btnBack;
    TextView txtjTitle;

    public static final String Default = "N/A";
    String uidHold;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_livematches, container, false);
        return layout;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView_live = layout.findViewById(R.id.recycler_live);
        txtNoLive = layout.findViewById(R.id.txtNoLive);
        txtNoLive.setVisibility(View.GONE);

        progressBar = layout.findViewById(R.id.progressBar);
        popuplayout=layout.findViewById(R.id.popuplayout);
        btnBack=layout.findViewById(R.id.btnBack);
        txtjTitle=layout.findViewById(R.id.txtjTitle);

        swipelayout= layout.findViewById(R.id.swipelayout);

        SharedPreferences prefs = getActivity().getSharedPreferences("userDetail", MODE_PRIVATE);
        uidHold = prefs.getString("uid", Default);



        if(!(uidHold==Default)) {
            loadKeys();
            progressBar.setVisibility(View.VISIBLE);
        }
        else
        {
            txtNoLive.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

        }

        swipelayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load();
                swipelayout.setRefreshing(false);

            }
        });

    }

    private void loadKeys() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("/UserMatches/" + uidHold);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                listkey.clear();
                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                    listkey.add(dataSnapshot.getKey());
                }

                if(listkey.size()>0) {
                    load();
                    progressBar.setVisibility(View.VISIBLE);
                }
                else
                {
                    txtNoLive.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void load() {

        try {
            loadFeatured loader = new loadFeatured();
            if (Looper.getMainLooper().getThread() == Thread.currentThread()) {
                Executor executor = Executors.newSingleThreadExecutor();
                executor.execute(loader);

            }

        } catch (Exception ignored) {
        }

    }

    private class loadFeatured extends Thread {

        @Override
        public void run() {
//            progress.setVisibility(View.GONE);
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference ref = database.getReference("Featured_match_predict/");


            ref.orderByChild("date").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {


                    list_live.clear();

                    if (firstTime_live) {


                        for (DataSnapshot dataSnapshot1 : snapshot.getChildren()) {


                            MatchesDetail listdata = new MatchesDetail();

                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.plan_price = String.valueOf(dataSnapshot1.child("planPrice").getValue());
                            listdata.desc = String.valueOf(dataSnapshot1.child("planDesc").getValue());
                            listdata.joinSts = "join";

                            if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("1") && (listkey.contains(dataSnapshot1.getKey()))) {
                                list_live.add(listdata);
                            } else {

                            }

                        }

                        getUpcomingData();
                        if(list_live!=null)
                        if(list_live.size()>0)
                        {

                                adapter_live.notifyDataSetChanged();

                        }


                    } else {


                        firstTime_live = true;

                        for (DataSnapshot dataSnapshot1 : snapshot.getChildren()) {


                            MatchesDetail listdata = new MatchesDetail();

                            listdata.nickname_1 = String.valueOf(dataSnapshot1.child("team1").getValue());
                            listdata.nickname_2 = String.valueOf(dataSnapshot1.child("team2").getValue());
                            listdata.match_id = String.valueOf(dataSnapshot1.getKey());
                            listdata.live = String.valueOf(dataSnapshot1.child("status").getValue());
                            listdata.series = String.valueOf(dataSnapshot1.child("series").getValue());
                            listdata.date_time = String.valueOf(dataSnapshot1.child("date").getValue());
                            listdata.plan_price = String.valueOf(dataSnapshot1.child("planPrice").getValue());
                            listdata.desc = String.valueOf(dataSnapshot1.child("planDesc").getValue());
                            listdata.joinSts = "join";

                            if (String.valueOf(dataSnapshot1.child("status").getValue()).equals("1") && (listkey.contains(dataSnapshot1.getKey()))) {
                                list_live.add(listdata);
                            } else {

                            }

                        }

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (list_live.size() > 0) {
                                    callRecycle_recent();
                                    progressBar.setVisibility(View.GONE);
                                } else {
//                                    Toast.makeText(getActivity(), "No Live Matches Available", Toast.LENGTH_SHORT).show();
                                    txtNoLive.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        });

//                        progress.setVisibility(View.GONE);
//                        data_layout.setVisibility(View.VISIBLE);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    progressBar.setVisibility(View.GONE);
                }

            });


        }
    }

    public void callRecycle_recent() {
        adapter_live = new Adapter_liveMatches(getContext(), getActivity(), getUpcomingData());
        recyclerView_live.setAdapter(adapter_live);
        recyclerView_live.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter_live.onselectClick(new Interface_live_my() {
            @Override
            public void onClick(String match_id, int position) {
                match_id_hold = match_id;

            }

        }, new Interface_viewDetail() {
            @Override
            public void onViewClick(int Position, String desc) {
                txtjTitle.setText(desc);
                showPopup();
            }
        });
    }

    public List<Object> getUpcomingData() {
        List<Object> data_upcoming = new ArrayList<>();

        for (int i = 0; i < list_live.size(); i++) {
            MatchesDetail info = new MatchesDetail();

            info = list_live.get(i);

            data_upcoming.add(info);
        }

        return data_upcoming;
    }

    @Override
    public void onResume() {
        if (firstTime_live) {
            load();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        firstTime_live = true;
    }


    @Override
    public void onClick(View v) {

    }

    public void showPopup() {

        popuplayout.setVisibility(View.VISIBLE);

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())


                .setContentHolder(new ViewHolder(popuplayout))
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                    }
                })
                .setExpanded(true, ViewGroup.LayoutParams.WRAP_CONTENT)
                .setGravity(Gravity.BOTTOM)
                .setMargin(0, 500, 0, 0)
//                .setInAnimation(R.anim.abc_fade_in)
//                .setOutAnimation(R.anim.abc_fade_out)


                .setContentBackgroundResource(R.color.colorPrimary_faint)// This will enable the expand feature, (similar to android L share dialog)
                .setCancelable(true)

                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialogPlus) {
                        popuplayout.setVisibility(View.GONE);
                    }
                })


                .create();

        dialog.show();


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }




}
